import "./App.css";
import "./index.css";
import "./sass/style.scss";
import Map from "./components/Map/map";
import RulesBoard from "./components/Rules/rulesBoard";
import Navbar from "./components/navbar";
import Home from "./components/home";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";
import LoginJwt from "./components/login/LoginJwt";
import SignUpJwt from "./components/signup/signupJwt";

function App() {

  return (
    <div className="App">
      <Router>
        <Navbar></Navbar>
        <div className="wrapper">
          <Switch>
            <Route path="/" exact component={Home}></Route>
            <Route path="/game" exact component={Map}></Route>
            <Route path="/rules" exact component={RulesBoard}></Route>
            <Route path="/login" exact component={LoginJwt}></Route>
            <Route path="/signup" exact component={SignUpJwt}></Route>
          </Switch>
        </div>
      </Router>
    </div>
  );
}
export default App;
