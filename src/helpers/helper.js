export const forEachSibling = (stateBoard, rowNumber, columnNumber, callback) => {
    if(rowNumber>=1){
        callback(stateBoard[rowNumber-1][columnNumber], rowNumber-1,columnNumber, 'topcenter')
    }
    if(columnNumber>=1){
        callback(stateBoard[rowNumber][columnNumber-1],rowNumber,columnNumber-1,'left')
    }
    if(stateBoard[rowNumber].length > columnNumber+1){
        callback(stateBoard[rowNumber][columnNumber+1],rowNumber,columnNumber+1,'right')
    }
    if(rowNumber<stateBoard.length-1){
        callback(stateBoard[rowNumber+1][columnNumber],rowNumber+1,columnNumber,'bottomcenter')
    }
}
