import React from "react";
const Rules = ({ text, pattern }) => {
  if (pattern === "#1C2833") {
    return (
      <div className="rules--card" style={{ backgroundColor: pattern }}>
        <div className="rules--box">
          <p style={{ color: "#ffff" }}>{text}</p>
        </div>
      </div>
    );
  } else if (pattern) {
    return (
      <div className="rules--card" style={{ backgroundColor: pattern }}>
        <div className="rules--box">
          <p>{text}</p>
        </div>
      </div>
    );
  } else {
    return (
      <div className="rules--card" style={{ backgroundColor: pattern }}>
        <div className="rules--box">
          <p>Vous êtes sur le point de départ</p>
        </div>
      </div>
    );
  }
};
export default Rules;
