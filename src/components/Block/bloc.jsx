import React, { useEffect } from "react";
import { useState } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import * as Icons from '@fortawesome/free-solid-svg-icons';
const Block = ({ onClick, row, column, pattern, currentPosition, direction }) => {
    const isActive = row === currentPosition.row && column === currentPosition.colum;
    const [flipY, setFlipY] = useState(false);
    const [flipX, setFlipX] = useState(false);
    const animateFlipY = () => {
        setFlipY(true);
        setTimeout(() => setFlipY(false), 250);
    }
    const animateFlipX = () => {
        setFlipX(true);
        setTimeout(() => setFlipX(false), 250);
    }
    useEffect(() => {
        if (direction === 'right' || direction === 'left') {
            animateFlipY()
        }
        if (direction === 'top' || direction === 'bottom') {
            animateFlipX()
        }

        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [pattern]);

    const handleClick = () => {
        onClick(row, column);
        if (isActive) {
            if (direction === 'right' || direction === 'left') {
                animateFlipY()
            }
            if (direction === 'top' || direction === 'bottom') {
                animateFlipX()
            }
        }
    }
    if (pattern === '#3FFCFE' || pattern === '#ECF0F1') {
        return (<div className="flip--tile">
            <div className={'flip--tile--inner ' + (flipY ? `flipY` : null) + ' ' + (flipX ? `flipX` : null)}>
                <div className="Block tile tile--front" onClick={handleClick} style={{ backgroundColor: pattern }}>
                    {pattern === '#3FFCFE' &&
                        (
                    <FontAwesomeIcon icon={Icons.faFlagCheckered}/>
                        ) 
                    }
                    {isActive && <FontAwesomeIcon icon={Icons.faTruck} style={{ color: '#000' }} />}
                </div>
            </div>
        </div>)
    }
    else {
        return (
            <div className="flip--tile">
                <div className={'flip--tile--inner ' + (flipY ? `flipY` : null) + ' ' + (flipX ? `flipX` : null)}>
                    <div className="Block tile tile--front" style={{ backgroundColor: pattern }} onClick={handleClick}>
                        {isActive && <FontAwesomeIcon icon={Icons.faTruck} style={{ color: '#fff' }} />}
                    </div>
                </div>
            </div>
        );
    }
};

export default Block;