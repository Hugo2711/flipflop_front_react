import { useState } from "react";
import { Link } from "react-router-dom";
import AuthService from "../services/auth.services";

const SignUpJwt= () => {
    const [email, setEmail] = useState("");
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");
  
  
    const handleSignUp = async (e) => {
      e.preventDefault();
      try {
        await AuthService.signupJwt(email,username, password).then(
          () => {
            window.location.href='/login';
          },
          (error) => {
            console.log(error);
          }
        );
      } catch (err) {
        console.log(err);
      }
    };
  return (
    <div className="login-wrapper">
      <h1>Sign Up</h1>
      <form className="login--form" onSubmit={handleSignUp}>
        <label>
          <p>Username</p>
          <input type="text" onChange={(e) => setUsername(e.target.value)} />
        </label>
        <label>
          <p>Email</p>
          <input type="text" onChange={(e) => setEmail(e.target.value)} />
        </label>
        <label>
          <p>Password</p>
          <input
            type="password"
            onChange={(e) => setPassword(e.target.value)}
          />
        </label>
        <div className="submit--btn">
          <button className="submitBtn" type="submit">
            Submit
          </button>
        </div>
        <Link to="/login" className="link--btn" >
        <p className="link--button">login</p>
        </Link>
      </form>
    </div>
  );
}
export default SignUpJwt
