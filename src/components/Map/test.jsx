// import React, { useState, useEffect } from "react";
// import Block from "./bloc";
// import { forEachSibling } from "../helpers/helper";
// import RulesBoard from "./rulesBoard";
// import Popup from "./popup";
// import { map } from "jquery";

// const mapTest = [[{}]];

// const MapTest = () => {
//   const [stateBoard, setStateBoard] = useState(mapTest);
//   const [previousPosition, setPreviousPosition] = useState({
//     colum: 0,
//     row: 0,
//   });
//   const [currentPosition, setCurrentPosition] = useState({ colum: 0, row: 0 });
//   const [count, setCount] = useState(0);
//   const [checkpoint, setCheckpoint] = useState(currentPosition);
//   const [isOpen, setIsOpen] = useState(false);
//   const [direction, setDirection] = useState(null);
//   const [boardSize, setBoardSize] = useState({ x: 10, y: 10 });
//   const [stateBloc, setStateBloc] = useState(70);
//   const [directionRandom, setDirectionRandom] = useState(null);
//   // const [initPosition, setInitPosition] = useState({ colum: 0, row: 0 });
//   let initPosition = { colum: 0, row: 0 };
//   const colors = [
//     { pattern: "#EC7063" },
//     { pattern: "#ABEBC6" },
//     { pattern: "#1C2833" },
//     { pattern: "#F9E79F" },
//     {pattern: "#3FFCFE" },
//     {pattern: "#ECF0F1" },
//     {pattern: "#F783FE" },
//     {},
//   ];
//   const ExistBefore = (prevRow, prevColum) => {
//     if (
//       mapTest.find(
//         (tile) => tile.colum === prevColum && tile.row === prevRow
//       ) &&
//       mapTest.find((tile) => tile.colum === prevColum && tile.row === prevRow)
//         .color
//     ) {
//       return true;
//     }
//     return false;
//   };
//   const getRandomDirection = () => {
//     const directionRandom = [];
//     if (
//       initPosition.row - 1 >= 0 &&
//       initPosition.row - 1 < boardSize.x &&
//       !ExistBefore(initPosition.row - 1, initPosition.colum)
//     ) {
//       directionRandom.push("top");
//     } else if (
//       initPosition.row + 1 >= 0 &&
//       initPosition.row + 1 < boardSize.x &&
//       !ExistBefore(initPosition.row + 1, initPosition.colum)
//     ) {
//       directionRandom.push("bot");
//     } else if (
//       initPosition.colum - 1 >= 0 &&
//       initPosition.colum - 1 < boardSize.y &&
//       !ExistBefore(initPosition.row, initPosition.colum - 1)
//     ) {
//       directionRandom.push("left");
//     } else if (
//       initPosition.colum + 1 >= 0 &&
//       initPosition.colum + 1 < boardSize.y &&
//       !ExistBefore(initPosition.row, initPosition.colum + 1)
//     ) {
//       directionRandom.push("right");
//     } else {
//       return "error";
//     }
//     console.log(directionRandom);
//     return directionRandom[Math.floor(Math.random() * directionRandom.length)];
//   };
//   console.log(mapTest);
//   const createMap = () => {
//     console.log("first init :", initPosition);
//     for (let i = 0; i < stateBloc; i++) {
//       const directionRandom = getRandomDirection();
//       let { row, colum } = initPosition;
//       console.log(directionRandom);
//       const color = getRandomColor();
//       if (directionRandom === "top") {
//         row -= 1;
//         initPosition = { colum, row };
//         // setInitPosition({ colum: colum, row: row - 1 });
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPreviousPosition();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "bot") {
//         row += 1;
//         initPosition = { colum, row };
//         //setInitPosition({ colum: colum, row: row });
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPreviousPosition();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "left") {
//         //setInitPosition({ colum: colum - 1, row: row });
//         colum -= 1;
//         initPosition = { colum, row };
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPreviousPosition();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "right") {
//         //setInitPosition({ colum: colum + 1, row: row });
//         colum += 1;
//         initPosition = { colum, row };
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPreviousPosition();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "error") {
//         initPosition = previousPosition;
//       }
//       console.log("index:", i);
//     }
//     console.log("after:", initPosition);
//     return mapTest;
//   };
//   const getRandomColor = () => {
//     return colors[Math.floor(Math.random() * colors.length)];
//   };

//   //function to check max size board
//   const togglePopup = () => {
//     setIsOpen(!isOpen);
//   };
//   const restart = () => {
//     setCurrentPosition({ colum: 0, row: 0 });
//     setCheckpoint({ colum: 0, row: 0 });
//     setCount(0);
//   };
//   window.addEventListener("load", (event) => {
//     createMap(0,0);
//   });
//   //fonction qui gère les pattern de couleurs
//   const getColorToChange = (columnNumber, rowNumber) => {
//     if (
//       ["#F783FE", "#ECF0F1", "#3FFCFE"].includes(
//         stateBoard[rowNumber][columnNumber].pattern
//       )
//     ) {
//       return stateBoard[rowNumber][columnNumber].pattern;
//     }
//     if (stateBoard[rowNumber][columnNumber].pattern === "#ABEBC6") {
//       return "#F9E79F";
//     } else if (stateBoard[rowNumber][columnNumber].pattern === "#F9E79F") {
//       return "#EC7063";
//     } else if (stateBoard[rowNumber][columnNumber].pattern === "#EC7063") {
//       return "#1C2833";
//     } else {
//       return "#ABEBC6";
//     }
//   };
//   //is allowed to click
//   const isAllow = (row, colum) => {
//     let allow = false;
//     forEachSibling(
//       stateBoard,
//       currentPosition.row,
//       currentPosition.colum,
//       (cell, rowNumber, columNumber, position) => {
//         if (row === rowNumber && colum === columNumber) {
//           allow = true;
//         }
//       }
//     );
//     return allow;
//   };
//   //counter des coups joués
//   const Counter = () => {
//     setCount(count + 1);
//   };
//   //handle checkpoint
//   useEffect(() => {
//     if (
//       stateBoard[currentPosition.row][currentPosition.colum].pattern ===
//       "#F783FE"
//     ) {
//       setCheckpoint(currentPosition);
//     }
//   }, [currentPosition, stateBoard]);
//   //Handle Click, isAllow, set position
//   const handleBlockClick = (rowNumber, columNumber) => {
//     if (!isAllow(rowNumber, columNumber)) {
//       return;
//     }

//     Counter();

//     //set current position
//     setPreviousPosition({
//       colum: currentPosition.colum,
//       row: currentPosition.row,
//     });
//     setCurrentPosition({ colum: columNumber, row: rowNumber });
//   };

//   // On position change
//   useEffect((row,colum) => {

//     //logique
//     const colorToChange = getColorToChange(colum, row);
//     const stateBoardPatern = stateBoard[row][colum].pattern;
//     if (currentPosition.colum - previousPosition.colum > 0) {
//       setDirection("right");
//     } else if (currentPosition.colum - previousPosition.colum < 0) {
//       setDirection("left");
//     } else if (currentPosition.row - previousPosition.row > 0) {
//       setDirection("bottom");
//     } else if (currentPosition.row - previousPosition.row < 0) {
//       setDirection("top");
//     }

//     //yellow case
//     if (stateBoardPatern === "#F9E79F") {
//       forEachSibling(
//         stateBoard,
//         row,
//         colum,
//         (cell, columNumber, rowNumber, position) => {
//           if (position === "topcenter") {
//             cell.pattern = getColorToChange(rowNumber, columNumber);
//           }
//           if (position === "right") {
//             cell.pattern = getColorToChange(rowNumber, columNumber);
//           }
//           if (position === "left") {
//             cell.pattern = getColorToChange(rowNumber, columNumber);
//           }
//           if (position === "bottomcenter") {
//             cell.pattern = getColorToChange(rowNumber, columNumber);
//           }
//         }
//       );
//       stateBoard[row][colum].pattern = colorToChange;
//     }
//     //green case
//     else if (stateBoardPatern === "#ABEBC6") {
//       stateBoard[row][colum].pattern = colorToChange;
//       setTimeout(() => {
//         handleGreenCase(row, colum);
//       }, 300);
//     }
//     //red case
//     else if (stateBoardPatern === "#EC7063") {
//       stateBoard[row][colum].pattern = colorToChange;
//       setTimeout(() => {
//         /*                 handleRedCase(row,colum);
//          */ setCurrentPosition(previousPosition);
//       }, 300);
//     }
//     //black case
//     else if (stateBoardPatern === "#1C2833") {
//       stateBoard[row][colum].pattern = colorToChange;
//       setCurrentPosition(checkpoint);
//     } else if (stateBoardPatern === "#3FFCFE") {
//       togglePopup();
//     }
//     setStateBoard([...stateBoard]);
//     // eslint-disable-next-line react-hooks/exhaustive-deps
//   }, [currentPosition]);
//   //handle Red case
//   /*  const handleRedCase = (rowNumber, columNumber) => {
//          const isGoingRight = columNumber - previousPosition.colum > 0;
//          const isGoingLeft = columNumber - previousPosition.colum < 0;
//          const isGoingTop = rowNumber - previousPosition.row < 0;
//          const isGoingBottom = rowNumber - previousPosition.row > 0;
//          if (isGoingBottom) {
//              if(rowNumber<stateBoard.length-1){
//              setCurrentPosition({ colum: columNumber, row: rowNumber + -1 });
//              }
//          }
//          if (isGoingRight) {
//              if(stateBoard[rowNumber].length > columNumber+1){
//              setCurrentPosition({ colum: columNumber-1, row: rowNumber });
//              }
//          }
//          if (isGoingLeft) {
//              if(columNumber>=1){
//              setCurrentPosition({ colum: columNumber +1, row: rowNumber  });
//              }
//          }
//          if (isGoingTop) {
//              if(rowNumber>=1){
//              setCurrentPosition({ colum: columNumber, row: rowNumber + 1 });
//              }
//          }
//      } */
//   //handle Green case
//   const handleGreenCase = (rowNumber, columNumber) => {
//     const isGoingRight = columNumber - previousPosition.colum > 0;
//     const isGoingLeft = columNumber - previousPosition.colum < 0;
//     const isGoingTop = rowNumber - previousPosition.row < 0;
//     const isGoingBottom = rowNumber - previousPosition.row > 0;
//     if (isGoingBottom) {
//       if (rowNumber < stateBoard.length - 1) {
//         setCurrentPosition({ colum: columNumber, row: rowNumber + 1 });
//       }
//     }
//     if (isGoingRight) {
//       if (stateBoard[rowNumber].length > columNumber + 1) {
//         setCurrentPosition({ colum: columNumber + 1, row: rowNumber });
//       }
//     }
//     if (isGoingLeft) {
//       if (columNumber >= 1) {
//         setCurrentPosition({ colum: columNumber - 1, row: rowNumber });
//       }
//     }
//     if (isGoingTop) {
//       if (rowNumber >= 1) {
//         setCurrentPosition({ colum: columNumber, row: rowNumber - 1 });
//       }
//     }
//   };
//   return (
//     <div>
//       <div className="appBoard">
//         <RulesBoard></RulesBoard>
//         <div className="container--board">
//           <div id="board">
//             {stateBoard.map((row, index) => {
//               return (
//                 <div className="row">
//                   {row.map((column, columnIndex) => {
//                     return (
//                       <Block
//                         onClick={handleBlockClick}
//                         row={index}
//                         column={columnIndex}
//                         currentPosition={currentPosition}
//                         pattern={column.pattern}
//                         previousPosition={previousPosition}
//                         direction={direction}
//                       ></Block>
//                     );
//                   })}
//                 </div>
//               );
//             })}
//           </div>
//           {isOpen && (
//             <Popup
//               content={
//                 <>
//                   <h2>You win !</h2>
//                   <p>Tu as gagné en {count} coups !</p>
//                   <button
//                     className="btn--replay"
//                     onClick={() => window.location.reload()}
//                   >
//                     REPLAY
//                   </button>
//                 </>
//               }
//               handleClose={togglePopup}
//             />
//           )}
//           <div className="container--bottom-board">
//             <div id="counterClic">Nombre de coup: {count}</div>
//             <button className="restart" onClick={restart}>
//               Restart
//             </button>
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };
// export default MapTest;
