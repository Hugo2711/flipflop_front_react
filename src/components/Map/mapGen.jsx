// import React, { useState, useEffect } from "react";
// import Block from "./bloc";
// import { forEachSibling } from "../helpers/helper";
// import RulesBoard from "./rulesBoard";
// import Popup from "./popup";
// import { map } from "jquery";
// const mapTest = [[{}]];

// const MapGen = () => {
//   const [stateBoard, setStateBoard] = useState(mapTest);
//   const [boardSize, setBoardSize] = useState({ x: 10, y: 10 });
//   const [stateBloc, setStateBloc] = useState(70);
//   const [directionRandom, setDirectionRandom] = useState(null);
//   const [previous, setPrevious]= useState({colum:0, row:0})
//   const [previousPosition, setPreviousPosition] = useState({
//     colum: 0,
//     row: 0,
//   });
//   const [currentPosition, setCurrentPosition] = useState({ colum: 0, row: 0 });
//   const [count, setCount] = useState(0);
//   const [checkpoint, setCheckpoint] = useState(currentPosition);
//   const [isOpen, setIsOpen] = useState(false);
//   const [direction, setDirection] = useState(null);
//   // const [initPosition, setInitPosition] = useState({ colum: 0, row: 0 });
//   let initPosition = { colum: 0, row: 0 };
//   const colors = [
//     { pattern: "#EC7063" },
//     { pattern: "#ABEBC6" },
//     { pattern: "#1C2833" },
//     { pattern: "#F9E79F" },
//     {},
//   ];
//   const ExistBefore = (prevRow, prevColum) => {
//     if (
//       mapTest.find(
//         (tile) => tile.colum === prevColum && tile.row === prevRow
//       ) &&
//       mapTest.find((tile) => tile.colum === prevColum && tile.row === prevRow)
//         .color
//     ) {
//       return true;
//     }
//     return false;
//   };
//   const getRandomDirection = () => {
//     const directionRandom = [];
//     if (
//       initPosition.row - 1 >= 0 &&
//       initPosition.row - 1 < boardSize.x &&
//       !ExistBefore(initPosition.row - 1, initPosition.colum)
//     ) {
//       directionRandom.push("top");
//     } else if (
//       initPosition.row + 1 >= 0 &&
//       initPosition.row + 1 < boardSize.x &&
//       !ExistBefore(initPosition.row + 1, initPosition.colum)
//     ) {
//       directionRandom.push("bot");
//     } else if (
//       initPosition.colum - 1 >= 0 &&
//       initPosition.colum - 1 < boardSize.y &&
//       !ExistBefore(initPosition.row, initPosition.colum - 1)
//     ) {
//       directionRandom.push("left");
//     } else if (
//       initPosition.colum + 1 >= 0 &&
//       initPosition.colum + 1 < boardSize.y &&
//       !ExistBefore(initPosition.row, initPosition.colum + 1)
//     ) {
//       directionRandom.push("right");
//     } else {
//       return "error";
//     }
//     console.log(directionRandom);
//     return directionRandom[Math.floor(Math.random() * directionRandom.length)];
//   };
//   console.log(mapTest);
//   const createMap = () => {
//     console.log("first init :", initPosition);
//     for (let i = 0; i < stateBloc; i++) {
//       const directionRandom = getRandomDirection();
//       let { row, colum } = initPosition;
//       console.log(directionRandom);
//       const color = getRandomColor();
//       if (directionRandom === "top") {
//         row -= 1;
//         initPosition = { colum, row };
//         // setInitPosition({ colum: colum, row: row - 1 });
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPrevious();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "bot") {
//         row += 1;
//         initPosition = { colum, row };
//         //setInitPosition({ colum: colum, row: row });
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPrevious();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "left") {
//         //setInitPosition({ colum: colum - 1, row: row });
//         colum -= 1;
//         initPosition = { colum, row };
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPrevious();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "right") {
//         //setInitPosition({ colum: colum + 1, row: row });
//         colum += 1;
//         initPosition = { colum, row };
//         console.log("row:", row);
//         console.log("colum:", colum);
//         console.log("in bot :", initPosition);
//         setPrevious();
//         mapTest.push({ color: color, row: row, colum: colum });
//       } else if (directionRandom === "error") {
//         initPosition = previous;
//       }
//       console.log("index:", i);
//     }
//     console.log("after:", initPosition);
//     return mapTest;
//   };
//   window.addEventListener("load", (event) => {
//     createMap();
//   });
//   const getRandomColor = () => {
//     return colors[Math.floor(Math.random() * colors.length)];
//   };
//   return (
//     <div>
//       <div className="appBoard">
//         <RulesBoard></RulesBoard>
//         <div className="container--board">
//           <div id="board">
//             {stateBoard.map((row, index) => {
//               return (
//                 <div className="row">
//                   {row.map((column, columnIndex) => {
//                     return (
//                       <Block
//                         //onClick={handleBlockClick}
//                         row={index}
//                         column={columnIndex}
//                         currentPosition={currentPosition}
//                         pattern={column.pattern}
//                         previousPosition={previousPosition}
//                         direction={direction}
//                       ></Block>
//                     );
//                   })}
//                 </div>
//               );
//             })}
//           </div>
//         </div>
//       </div>
//     </div>
//   );
// };

// export default MapGen;
