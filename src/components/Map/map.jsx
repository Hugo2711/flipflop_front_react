import React, { useState, useEffect } from "react";
import Block from "../Block/bloc";
import { forEachSibling } from "../../helpers/helper";
import Popup from "../popup";
const map = [
  [
    { pattern: "#ECF0F1" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#F783FE" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#F783FE" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#F9E79F" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#3FFCFE" },
  ],
];
const Map = () => {
  const [stateBoard, setStateBoard] = useState(map);
  const [previousPosition, setPreviousPosition] = useState({
    colum: 0,
    row: 0,
  });
  const [currentPosition, setCurrentPosition] = useState({ colum: 0, row: 0 });
  const [count, setCount] = useState(0);
  const [checkpoint, setCheckpoint] = useState(currentPosition);
  const [isOpen, setIsOpen] = useState(false);
  const [direction, setDirection] = useState(null);
  const togglePopup = () => {
    setIsOpen(!isOpen);
  };
  const restart = () => {
    setCurrentPosition({ colum: 0, row: 0 });
    setCheckpoint({ colum: 0, row: 0 });
    setCount(0);
  };

  const getColorToChange = (columnNumber, rowNumber) => {
    if (
      ["#F783FE", "#ECF0F1", "#3FFCFE"].includes(
        stateBoard[rowNumber][columnNumber].pattern
      )
    ) {
      return stateBoard[rowNumber][columnNumber].pattern;
    }
    if (stateBoard[rowNumber][columnNumber].pattern === "#ABEBC6") {
      return "#F9E79F";
    } else if (stateBoard[rowNumber][columnNumber].pattern === "#F9E79F") {
      return "#EC7063";
    } else if (stateBoard[rowNumber][columnNumber].pattern === "#EC7063") {
      return "#1C2833";
    } else {
      return "#ABEBC6";
    }
  };
  const isAllow = (row, colum) => {
    let allow = false;
    forEachSibling(
      stateBoard,
      currentPosition.row,
      currentPosition.colum,
      (cell, rowNumber, columNumber, position) => {
        if (row === rowNumber && colum === columNumber) {
          allow = true;
        }
      }
    );
    return allow;
  };
  const Counter = () => {
    setCount(count + 1);
  };

  useEffect(() => {
    if (
      stateBoard[currentPosition.row][currentPosition.colum].pattern ===
      "#F783FE"
    ) {
      setCheckpoint(currentPosition);
    }
  }, [currentPosition, stateBoard]);
  const handleBlockClick = (rowNumber, columNumber) => {
    if (!isAllow(rowNumber, columNumber)) {
      return;
    }

    Counter();

    setPreviousPosition({
      colum: currentPosition.colum,
      row: currentPosition.row,
    });
    setCurrentPosition({ colum: columNumber, row: rowNumber });
  };

  useEffect(() => {
    const { row, colum } = currentPosition;

    const colorToChange = getColorToChange(colum, row);
    const stateBoardPatern = stateBoard[row][colum].pattern;
    if (currentPosition.colum - previousPosition.colum > 0) {
      setDirection("right");
    } else if (currentPosition.colum - previousPosition.colum < 0) {
      setDirection("left");
    } else if (currentPosition.row - previousPosition.row > 0) {
      setDirection("bottom");
    } else if (currentPosition.row - previousPosition.row < 0) {
      setDirection("top");
    }

    if (stateBoardPatern === "#F9E79F") {
      forEachSibling(
        stateBoard,
        row,
        colum,
        (cell, columNumber, rowNumber, position) => {
          if (position === "topcenter") {
            cell.pattern = getColorToChange(rowNumber, columNumber);
          }
          if (position === "right") {
            cell.pattern = getColorToChange(rowNumber, columNumber);
          }
          if (position === "left") {
            cell.pattern = getColorToChange(rowNumber, columNumber);
          }
          if (position === "bottomcenter") {
            cell.pattern = getColorToChange(rowNumber, columNumber);
          }
        }
      );
      stateBoard[row][colum].pattern = colorToChange;
    } else if (stateBoardPatern === "#ABEBC6") {
      stateBoard[row][colum].pattern = colorToChange;
      setTimeout(() => {
        handleGreenCase(row, colum);
      }, 300);
    } else if (stateBoardPatern === "#EC7063") {
      stateBoard[row][colum].pattern = colorToChange;
      setTimeout(() => {
        setCurrentPosition(previousPosition);
      }, 300);
    } else if (stateBoardPatern === "#1C2833") {
      stateBoard[row][colum].pattern = colorToChange;
      setCurrentPosition(checkpoint);
    } else if (stateBoardPatern === "#3FFCFE") {
      togglePopup();
    }
    setStateBoard([...stateBoard]);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPosition]);
  const handleGreenCase = (rowNumber, columNumber) => {
    const isGoingRight = columNumber - previousPosition.colum > 0;
    const isGoingLeft = columNumber - previousPosition.colum < 0;
    const isGoingTop = rowNumber - previousPosition.row < 0;
    const isGoingBottom = rowNumber - previousPosition.row > 0;
    if (isGoingBottom) {
      if (rowNumber < stateBoard.length - 1) {
        setCurrentPosition({ colum: columNumber, row: rowNumber + 1 });
      }
    }
    if (isGoingRight) {
      if (stateBoard[rowNumber].length > columNumber + 1) {
        setCurrentPosition({ colum: columNumber + 1, row: rowNumber });
      }
    }
    if (isGoingLeft) {
      if (columNumber >= 1) {
        setCurrentPosition({ colum: columNumber - 1, row: rowNumber });
      }
    }
    if (isGoingTop) {
      if (rowNumber >= 1) {
        setCurrentPosition({ colum: columNumber, row: rowNumber - 1 });
      }
    }
  };
  return (
    <div>
      <div className="appBoard">
        <div className="container--board">
          <div id="board">
            {stateBoard.map((row, index) => {
              return (
                <div className="row">
                  {row.map((column, columnIndex) => {
                    return (
                      <Block
                        onClick={handleBlockClick}
                        row={index}
                        column={columnIndex}
                        currentPosition={currentPosition}
                        pattern={column.pattern}
                        previousPosition={previousPosition}
                        direction={direction}
                      ></Block>
                    );
                  })}
                </div>
              );
            })}
          </div>
          {isOpen && (
            <Popup
              content={
                <>
                  <h2>You win !</h2>
                  <p>Tu as gagné en {count} coups !</p>
                  <button
                    className="btn--replay"
                    onClick={() => window.location.reload()}
                  >
                    REPLAY
                  </button>
                </>
              }
              handleClose={togglePopup}
            />
          )}
          <div className="container--bottom-board">
            <div id="counterClic">Nombre de coup: {count}</div>
            <button className="restart" onClick={restart}>
              Restart
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
export default Map;
