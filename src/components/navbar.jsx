import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import authService from "./services/auth.services";
const Navbar = () => {
  const [currentUser, setCurrentUser] = useState(undefined);
  useEffect(() => {
    const user = authService.getCurrentUser();
    if (user) {
      setCurrentUser(user);
    }
  }, []);
  const logout = () => {
    authService.logout();
  };
  return (
    <nav className="navbar--flex">
      <div className="nav--container">
        <div className="nav--left">
          <div className="nav--header">
            <span className="Logo">FlipFlop</span>
          </div>
          <div class="hamburger">
            <div class="line1"></div>
            <div class="line2"></div>
            <div class="line3"></div>
          </div>
          <div className="nav--box">
            <Link to="/" className="nav--link">
              Home
            </Link>
          </div>
          <div className="nav--box">
            <Link to="/game" className="nav--link">
              game
            </Link>
          </div>
          <div className="nav--box">
            <Link to="/rules" className="nav--link">
              Rules
            </Link>
          </div>
        </div>
        <div className="nav--right">
          {currentUser ? (
            <div className="nav--box">
              <Link to="/" className="nav--link" onClick={logout}>
                Logout
              </Link>
            </div>
          ) : (
            <div className="nav--box">
              <Link to="/signup" className="nav--button">
                <button className="sign--button">SignUp</button>
              </Link>
              <Link to="/login" className="nav--button">
                <button className="login--button">Login</button>
              </Link>
            </div>
          )}
        </div>
      </div>
    </nav>
  );
};
export default Navbar;
