import axios from "axios";

const API_URL = "http://127.0.0.1:8001";

const signupJwt = async (email,username, password) => {
  const response = await axios
        .post(API_URL + "/register", {
            email,
            username,
            password,
        });
    if (response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
    }
    return response.data;
};
const loginJwt = async (username, password) => {
  const response = await axios
        .post(API_URL + "/api/login_check", {
            username,
            password,
        });
    if (response.data.token) {
        localStorage.setItem("user", JSON.stringify(response.data));
    }
    return response.data;
};

const logout = () => {
  localStorage.removeItem("user");
  window.location.href="/";
};

const getCurrentUser = () => {
  return JSON.parse(localStorage.getItem("user"));
  
};

const authService = {
  signupJwt,
  loginJwt,
  logout,
  getCurrentUser,
};

export default authService;