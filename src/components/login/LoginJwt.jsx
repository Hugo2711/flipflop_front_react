import React, { useState } from "react";
import { Link } from "react-router-dom";
import AuthService from "../services/auth.services";

const LoginJwt = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");


  const handleLogin = async (e) => {
    e.preventDefault();
    try {
      await AuthService.loginJwt(username, password).then(
        () => {
          window.location.href='/';
        },
        (error) => {
          console.log(error);
        }
      );
    } catch (err) {
      console.log(err);
    }
  };
  return(
    <div className="login-wrapper">
      <h1>Log In</h1>
      <form className="login--form" onSubmit={handleLogin}>
        <label>
          <p>Username</p>
          <input type="text" onChange={e => setUsername(e.target.value)}/>
        </label>
        <label>
          <p>Password</p>
          <input type="password" onChange={e => setPassword(e.target.value)}/>
        </label>
        <div className='submit--btn'>
          <button className="submitBtn" type="submit">Submit</button>
        </div>
        <Link to="/signup" className="link--btn">
              <p className="link--button">SignUp</p>
        </Link>
      </form>
    </div>
  )
}
export default LoginJwt;