import React from "react";
import { Link } from "react-router-dom";
const stateBoardHome = [
  [
    { pattern: "#ECF0F1" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#F783FE" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F9E79F" },
    { pattern: "#F783FE" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#F9E79F" },
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#F9E79F" },
  ],
  [
    { pattern: "#ABEBC6" },
    { pattern: "#EC7063" },
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#F9E79F" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
  ],
  [
    { pattern: "#1C2833" },
    { pattern: "#F9E79F" },
    { pattern: "#EC7063" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#F783FE" },
    { pattern: "#1C2833" },
    { pattern: "#1C2833" },
    { pattern: "#EC7063" },
    { pattern: "#ABEBC6" },
    { pattern: "#3FFCFE" },
  ],
];
const Home = () => {
  return (
    <div className="home--container">
      <div className="home--title">
        <h1>Welcome on FlipFlop game</h1>
        <p>Discover an original game</p>
        <Link to="/rules" className="home--btn">
          Get started
        </Link>
      </div>
      <div className="appBoard">
        <div className="container--board opacity--home">
          <div id="board">
            {stateBoardHome.map((row) => {
              return (
                <div className="row">
                  {row.map((column) => {
                    return (
                      <div className="flip--tile">
                        <div className="flip--tile--inner null null">
                          <div
                            className="Block tile tile--front"
                            style={{
                              backgroundColor: column.pattern,
                            }}
                          ></div>
                        </div>
                      </div>
                    );
                  })}
                </div>
              );
            })}
          </div>
        </div>
      </div>
    </div>
  );
};
export default Home;
